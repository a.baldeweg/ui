# Changelog

## 5.2

- Update Dependencies to the latest version
- Require NodeJS 12 or later

## 5.1.0

- masthead now 50px instead of 33px
- `.masthead_btn` replaces buttons in masthead
- `.notifications` are wider

## 5.0.0

- Removed deprecations
- Improvements in component card, Search and in some global styles
- Forms cleaned up, error classes removed
- <hr> needs now class `.divider` added
- Dark Mode added, but still in development
- Custom Properties renamed

## 4.4.0

- Links in the contextMenu will look like the other elements
- Added settings to masthead
- Notifications got background color and border not only in the left
- New class `.offcanvas_container` and an overlay
- Deprecated class `.icon_branded`
- Deprecated `.nav`
- `--color-brand` deprecated, use `--color-primary` instead
- `-color-backgroundLight` renamed to `--color-background-dark-1`
- `--color-backgroundDark` renamed to `--color-background-dark-2`
- CSS custom property names changed to kebab case
- Deprecated class `.hamburger`
- `.container` replaces `.section`
  - `section_fixed` -> `container_m`
  - `section_fluid` -> `container_l`
- The search component comes with a new reset button
