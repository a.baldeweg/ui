# baldeweg_ui

baldeweg_ui is a CSS framework.

## Install

Run `yarn add baldeweg_ui`

## Dev

Run `yarn watch` to start the development environment.

## Build

Run `yarn build` to start a new build process.
